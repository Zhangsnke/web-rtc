# webRTC

#### 介绍
基于HTML5 + WebRTC 进行多人视频交流，后端采用JAVA。局域网实现视频通讯，NAT穿透需要自己搭建

#### 软件架构
软件架构说明
HTML5 + WebRTC + SpringBoot，后端Java websocket

#### 安装教程

1.  使用外置Tomcat,Maven依赖，通过maven导入依赖
2.  配置外置Tomcat
3.  访问http://localhost:8080/webrtc/login/index(如果想使用Chrome访问，请先修改video.js，写法有些区别)

#### 使用说明

1.  目前可以多人聊天，但是存在回音，需要修改（这部分改动比较大，因为为简单demo就不展开操作）
2.  需要NAT穿透（内网没关系），这部分NAT穿透搭建请自行操作
3.  demo只是用来验证可行性，并不适合商用，而且后端CPU压力很大，也没有配置解码，需要自行实现

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
